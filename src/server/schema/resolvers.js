//Please add all the resolvers at this file for now
import {merge} from 'lodash';
import {MongoClient, ObjectId} from 'mongodb'
import userResolver from './user/user.resolver';
import taskResolver from './task/task.resolver';

// Use this resolver when required for defining the root resolver
const resolvers = merge(userResolver, taskResolver);

export default resolvers;
