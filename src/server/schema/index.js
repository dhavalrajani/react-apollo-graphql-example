import {User, createUser, loginUser, readUser, updateUser, deleteUser} from './user';
import {Task, addTask, multipleTodos, singleTodo, updateTask, deleteTask} from './task';

module.exports = {
    User: User,
    Task: Task,
    createUser : createUser,
    loginUser : loginUser,
    readUser : readUser,
    updateUser : updateUser,
    deleteUser : deleteUser,
    addTask : addTask,
    multipleTodos : multipleTodos,
    singleTodo : singleTodo,
    updateTask : updateTask,
    deleteTask : deleteTask
}
