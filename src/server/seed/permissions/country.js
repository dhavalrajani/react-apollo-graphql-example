function countryPermissionSet() {
    return [
        {
            "userType": "admin",
            "C": true,
            "R": true,
            "U": true,
            "D": true
        }, {
            "userType": "endUser",
            "C": true,
            "R": false,
            "U": false,
            "D": false
        }
    ];
}

module.exports = countryPermissionSet;