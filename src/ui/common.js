var RsTabularConfig =[
    {
        tableName:"tasks",
        tableConfig:[
            {data:"taskName",title:"Task name"},
            {data:"Author",title:"Author Email",render:function (cell,row){
                return row.Author.email;
            }}
        ]
    }
];
export default RsTabularConfig;