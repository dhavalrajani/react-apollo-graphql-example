import React, {Component} from 'react';
import "font-awesome/css/font-awesome.css";
import FontAwesome from "react-fontawesome";


class AdminFooterContent extends Component {
    render() {
        return (

            <div className="footer-main">
                <div className="footer-wrap">
                    <div className="footer-wrap-innr">Copyright &copy; 2017, hcmsprint, Division of raksan</div>

                </div>
                <div className="social-media pull-right">
                    <FontAwesome name='facebook'/>
                    <FontAwesome name='twitter'/>
                    <FontAwesome name='google-plus'/>
                    <FontAwesome name='linkedin'/>
                </div>
            </div>

        )
    }
}

export default AdminFooterContent;
