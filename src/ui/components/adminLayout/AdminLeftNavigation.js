import React,{Component} from 'react';
import $ from "jquery";

/*import {loginContainer} from '../../common/containers/login'
import {logout} from '../../common/containers/logout'*/
/*import ScrollArea from 'react-scrollbar'*/


class AdminLeftNavigationContent extends Component{
  componentDidMount()
  {
      $('.left-nav-wrap .left-nav ul li').click(function(){
        $(this).addClass('active')
        $(this).siblings().removeClass('active')
      })


      $('.hamburger').click(function(){
        $('.left-nav-wrap .left-nav').toggleClass('active');
      });

  }

  render(){
    return (


      <div className="left-nav-wrap">
        <div className="left-nav">
          <ul>
            <li className="active"><a href="/admin/platformDashboard"><figure><img src="/images/dashboard.png" /></figure><label>dashboard</label></a></li>
            <li><a href="/admin/rmDetails"><figure><img src="/images/relation.png" /></figure><label>relationship management</label></a></li>
            <li><a href="/admin/TransactionRequestedDetails"><figure><img src="/images/transaction.png" /></figure><label>transactions</label></a></li>
            <li><a href="/admin/platformSettings"><figure><img src="/images/settings.png" /></figure><label>settings</label></a></li>
            <li><a href="/admin/platformDocuments"><figure><img src="/images/documents.png" /></figure><label>documents</label></a></li>
            <li><a href="/admin/platformTemplates"><figure><img src="/images/templet.png" /></figure><label>template</label></a></li>
            <li><a href="/admin/platformConversation"><figure><img src="/images/chat.png" /></figure><label>chat</label></a></li>
            <li><a href="#"><figure><img src="/images/box.png" /></figure><label>my profile</label></a></li>
          </ul>
        </div>
      </div>

    )
  }
}
export default AdminLeftNavigationContent;
