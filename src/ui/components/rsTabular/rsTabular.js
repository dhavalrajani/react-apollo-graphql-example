import React,{Component} from "react";
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
const products = [];
import RsTabularConfig from "../../common.js";
import "../stylesheets/css/plugins/react-bootstrap-table.css";
function normalFormatted(cell,row){
    return cell;
}
class RsTabular extends Component{
    constructor(props){
        super(props);
    }
    getTableHeaders(){
        var tableName = this.props.tableName;
        var index = RsTabularConfig.findIndex(function (d){
            if(d.tableName == tableName){
                return d;
            }
        });
        if(index != -1){
            var config = RsTabularConfig[index].tableConfig;

            return config.map((value,index)=>{
                return <TableHeaderColumn
                    dataField={value.data}
                    isKey={index == 0 ? true : false}
                    dataFormat={value.render ? value.render : normalFormatted}
                >{value.title}</TableHeaderColumn>
            })
        }
    }


    render(){
        return (

            <BootstrapTable data={ this.props.tableData }>
                {this.getTableHeaders()}
            </BootstrapTable>
        );
    }
}


export default RsTabular;